# Todo
一个非常简洁清爽的清单工具，帮您轻松记录个人计划。本App的特色就是简洁，让人一目了然，在交互上让人体会到视觉上的舒适感。固定的4种清单分类在顶部可直接切换列表。API由WanAndroid提供

[去fir下载](https://fir.im/wantodo)

## Screenshots
<div >
<img  src="https://raw.githubusercontent.com/xujiaji/xujiaji.github.io/pictures/todo/screen/a.png" width="24%" height="auto">
<img style="margin-left:0px;" src="https://raw.githubusercontent.com/xujiaji/xujiaji.github.io/pictures/todo/screen/b.png" width="24%" >
<img style="margin-left:0px;" src="https://raw.githubusercontent.com/xujiaji/xujiaji.github.io/pictures/todo/screen/c.png" width="24%" >
<img  src="https://raw.githubusercontent.com/xujiaji/xujiaji.github.io/pictures/todo/screen/d.png" width="24%" height="auto">
</div>

<div >
<img  src="https://raw.githubusercontent.com/xujiaji/xujiaji.github.io/pictures/todo/screen/e.png" width="24%" height="auto">
<img style="margin-left:0px;" src="https://raw.githubusercontent.com/xujiaji/xujiaji.github.io/pictures/todo/screen/f.png" width="24%" >
<img style="margin-left:0px;" src="https://raw.githubusercontent.com/xujiaji/xujiaji.github.io/pictures/todo/screen/g.png" width="24%" >
<img  src="https://raw.githubusercontent.com/xujiaji/xujiaji.github.io/pictures/todo/screen/h.png" width="24%" height="auto">
</div>

## License
> Copyright (C) 2018 Xu Jiaji  
> Licensed under the [GPL-3.0](https://www.gnu.org/licenses/gpl.html) license.  
> (See the [LICENSE](https://github.com/xujiaji/Todo/blob/master/LICENSE) file for the whole license text.)
